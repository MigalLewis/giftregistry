import { Component, OnInit, NgZone, AfterContentChecked } from '@angular/core';
import { faGoogle } from '@fortawesome/free-brands-svg-icons'
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../services/alert.service';
import { LoadingService } from '../services/loading.service';
import { AuthService } from '../services/authService';
import { User } from '../models/user.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterContentChecked {
  faGoogle = faGoogle;
  emailForm: FormGroup;


  constructor(private angularFireAuth: AngularFireAuth,
     private router:Router,
     private zone: NgZone,
     private alertService: AlertService,
     private loadingService: LoadingService,
     private authService: AuthService,
     private fb: FormBuilder) {
      this.loadingService.startSpinner();
      this.createForm();
    }
  ngAfterContentChecked(): void {
    this.loadingService.stopSpinner();
  }

  ngOnInit(): void {

  }

  createForm() {
    this.emailForm = this.fb.group({
      email: ['', Validators.required ]
    });
  }
  onPasswordlessLogin() {
    let actionCodeSettings = {
      url: environment.auth.returnUrl,
      handleCodeInApp: true
    };
    this.angularFireAuth.sendSignInLinkToEmail(this.emailForm.value.email, actionCodeSettings).then(data =>{
      this.alertService.addAlert({type: 'success', message: 'Please check you email for link'});
      window.localStorage.setItem('emailForSignIn', this.emailForm.value.email);
    }).catch(error => {
      this.alertService.addAlert({type: 'error', message: error});
    });
  }
  onLoginWithGoogle() {
    this.angularFireAuth.signInWithPopup(new auth.GoogleAuthProvider()).then((data: auth.UserCredential)=>{
      let user: User = {
        name: data.user.displayName
      }
      this.authService.addUser(user,data.user.uid).then(data => {
        window.localStorage.setItem('user',  JSON.stringify(user));
      });
      this.zone.run(() => {
        this.router.navigate(['/registry']);
      });
    });
  }



}
