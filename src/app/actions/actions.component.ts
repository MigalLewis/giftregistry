import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { faUsers, 
         faShoppingBasket, 
         faHeart, 
         faShoppingCart,
         faLock} from '@fortawesome/free-solid-svg-icons';
import { Item } from '../models/item.model';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements OnInit {
  faUsers = faUsers;
  faShoppingBasket = faShoppingBasket;
  faHeart = faHeart;
  faShoppingCart = faShoppingCart;
  faLock = faLock;

  @Output() showInterest: EventEmitter<boolean>;
  @Output() bought: EventEmitter<boolean>;
  @Output() groupBuy: EventEmitter<boolean>;
  @Output() like: EventEmitter<boolean>;
  @Input() item: Item;

  constructor() {
    this.showInterest = new EventEmitter();
    this.bought = new EventEmitter();
    this.groupBuy = new EventEmitter();
    this.like = new EventEmitter();
   }

  ngOnInit(): void {
  }

  onShowInterest() {
    this.showInterest.emit(true);
  }
  onBought() {
    this.bought.emit(true);
  }
  onGroupBuy() {
    this.groupBuy.emit(true);
  }
  onLike() {
    this.like.emit(true);
  }
  isAvailable() {
    return !this.item.available;
  }

  unlimited() {
    return this.item.quantity === -1;
  }

}
