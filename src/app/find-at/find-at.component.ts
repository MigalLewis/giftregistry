import { Component, OnInit, Input } from '@angular/core';
import { Shop } from '../models/shop.model';

@Component({
  selector: 'app-find-at',
  templateUrl: './find-at.component.html',
  styleUrls: ['./find-at.component.scss']
})
export class FindAtComponent implements OnInit {
  @Input() shopList: Shop[];
  @Input() unlimited: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
