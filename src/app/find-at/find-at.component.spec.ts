import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindAtComponent } from './find-at.component';

describe('FindAtComponent', () => {
  let component: FindAtComponent;
  let fixture: ComponentFixture<FindAtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindAtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindAtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
