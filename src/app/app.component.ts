import { Component } from '@angular/core';
import { RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError, Router } from '@angular/router';
import { Alert } from './models/alert.model';
import { AlertService } from './services/alert.service';
import { faCheck, faTimes, faExclamationTriangle, faInfo } from '@fortawesome/free-solid-svg-icons';
import { LoadingService } from './services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'giftRegistry';
  loadingOverlay: boolean;
  alerts: Alert[];
  faIcon = faCheck;

  constructor(private router: Router,
              private alertService: AlertService,
              private loadingService: LoadingService) {
    this.alerts = [];
    this.alertService.getAlerts().subscribe((alert: Alert) => {
      this.setFontAwesomeType(alert.type);
      this.alerts.push(alert);
      setTimeout(() => this.close(alert), 10000);
    });
    this.loadingService.isLoading().subscribe( data => {
      this.loadingOverlay = data; });
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loadingOverlay = true;
    }
    if (event instanceof NavigationEnd) {
      this.loadingOverlay = false;
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loadingOverlay = false;
    }
    if (event instanceof NavigationError) {
      this.loadingOverlay = false;
    }
  }

  close(alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }
  setFontAwesomeType(type: string) {
    switch (type) {
      case 'success' : this.faIcon = faCheck;
                       break;
      case 'danger' : this.faIcon = faTimes;
                      break;
      case 'warning' : this.faIcon = faExclamationTriangle;
                       break;
      default: this.faIcon = faInfo;
    }
  }
}
