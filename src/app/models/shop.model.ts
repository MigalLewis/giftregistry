export class Shop {
    name: string;
    link: string;
    image: string;
}