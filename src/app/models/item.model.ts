import { Shop } from './shop.model';

export class Item {
    id: string;
    name: string;
    description: string;
    image: string;
    shops: Shop[];
    available?: boolean;
    locked: LockedItem;
    likes?: number;
    groupBuy: boolean;
    quantity?: number; 
}
 export class LockedItem {
     userId?: string;
     locked: boolean;
     dateLocked?: Date;
 }

 export class Group {
    itemId: string;
    members: Member[]
 }
 
 export class Member {
    name: string;
    amount: string;
    phoneNumber: string;
}