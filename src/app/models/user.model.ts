import { Item } from './item.model';

export class User {
    name: string;
    boughtItem?: Item;
    likedItems?: Item[];
    lockedItems?: Item[];
}
