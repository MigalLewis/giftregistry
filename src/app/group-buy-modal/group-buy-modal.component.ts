import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { Item, Group, Member } from '../models/item.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { RegistryService } from '../services/registry.service';

@Component({
  selector: 'app-group-buy-modal',
  templateUrl: './group-buy-modal.component.html',
  styleUrls: ['./group-buy-modal.component.scss']
})
export class GroupBuyModalComponent implements OnInit, OnChanges {
  @Input() item: Item;
  @Input() group: Group;
  @Output() addToList: EventEmitter<boolean>;
  @Output() exit: EventEmitter<boolean>;

  faShoppingCart = faShoppingCart;

  state: string;
  groupForm: FormGroup;

  
  constructor(private fb: FormBuilder,
              private registryService: RegistryService) { 
    this.addToList =new EventEmitter();
    this.exit = new EventEmitter();
    this.state = 'initial'
    this.createForm();
  }
  ngOnChanges(changes: SimpleChanges): void {
  }

  ngOnInit(): void {
  }

  onAddToList() {
    this.addToList.emit(true);
  }
  startGroup() {
    this.state = 'start';
    this.group = {
      itemId: this.item.id,
      members: []
    }
  }
  createGroup() {
    this.item.groupBuy = true;
    this.registryService.createGroup(this.group, this.item).
      then(data=> this.joinGroup());
  }
  inquireToJoinGroup() {
    this.state = 'join';
  }
  joinGroup() {
    let member: Member = {
      name: this.groupForm.value.name,
      amount: this.groupForm.value.amount,
      phoneNumber: this.groupForm.value.cellphone
    };
    this.group.members.push(member);
    this.state = 'initial';
    this.registryService.joinGroup(this.group, this.item.id);
  }

  createForm() {
    this.groupForm = this.fb.group({
      name: ['', Validators.required ],
      cellphone: ['', Validators.required ],
      amount: ['', Validators.required ]
    });
  }

  onExit(event) {
    this.exit.emit(event);
  }
  

}
