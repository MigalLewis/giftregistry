import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupBuyModalComponent } from './group-buy-modal.component';

describe('GroupBuyModalComponent', () => {
  let component: GroupBuyModalComponent;
  let fixture: ComponentFixture<GroupBuyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupBuyModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupBuyModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
