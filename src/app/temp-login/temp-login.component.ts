import { Component, OnInit, NgZone } from '@angular/core';
import { LoadingService } from '../services/loading.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-temp-login',
  templateUrl: './temp-login.component.html',
  styleUrls: ['./temp-login.component.scss']
})
export class TempLoginComponent implements OnInit {

  constructor(private loadingService:LoadingService,
    private angularFireAuth: AngularFireAuth,
    private router:Router,
    private zone: NgZone,
    private alertService: AlertService) { }

  ngOnInit(): void {
    const url = this.router.url;
    this.confirmSignIn(url);
  }

  async confirmSignIn(url) {
    this.loadingService.startSpinner();
    try {
      let email = window.localStorage.getItem('emailForSignIn');
      if (url && email && this.angularFireAuth.isSignInWithEmailLink(url)) {
        this.angularFireAuth.signInWithEmailLink(email, url).then(data => {
          window.localStorage.removeItem('emailForSignIn');
          this.loadingService.stopSpinner();
          this.router.navigate(['/registry']);
        });
      }
    } catch (error) {
      this.alertService.addAlert({type: 'error', message: error});
      this.loadingService.stopSpinner();
    }
  }

}
