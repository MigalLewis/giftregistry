import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-action-modal',
  templateUrl: './action-modal.component.html',
  styleUrls: ['./action-modal.component.scss']
})
export class ActionModalComponent implements OnInit {
  @Input() title: string;
  @Output() exit: EventEmitter<boolean>;


  constructor() {
    this.exit = new EventEmitter();
  }

  ngOnInit(): void {
    
  }

  onExitModal() {
    this.exit.emit(true);
  }

}
