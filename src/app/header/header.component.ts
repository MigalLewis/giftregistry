import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user: User;

  constructor(public auth: AngularFireAuth) {
    this.auth.user.subscribe(data => {
      this.user = data;
    });
   }

  ngOnInit(): void {
  }

  logout() {
    this.auth.signOut();
  }
  

  getInitials() {
    if(this.user && this.user.displayName) {
      let names = this.user.displayName.split(' '),
      initials = names[0].substring(0, 1).toUpperCase();
      if (names.length > 1) {
          initials += names[names.length - 1].substring(0, 1).toUpperCase();
      }
      return initials;
    }
    return '';
  }

}
