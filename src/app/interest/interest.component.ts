import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faHeart, faLock } from '@fortawesome/free-solid-svg-icons';
import { from } from 'rxjs';
import { Item } from '../models/item.model';

@Component({
  selector: 'app-interest',
  templateUrl: './interest.component.html',
  styleUrls: ['./interest.component.scss']
})
export class InterestComponent implements OnInit {
  @Input() item: Item;
  @Output() lock: EventEmitter<Item>;
  @Output() exit: EventEmitter<boolean>;
  faLike= faHeart;
  faLock= faLock;

  constructor() {
    this.lock = new EventEmitter();
    this.exit = new EventEmitter();
  }

  ngOnInit(): void {
    
  }

  onLock() {
    this.lock.emit(this.item);
  }
  onExit(event) {
    this.exit.emit(event);
  }

}
