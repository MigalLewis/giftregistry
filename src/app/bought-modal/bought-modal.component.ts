import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { Item } from '../models/item.model';


@Component({
  selector: 'app-bought-modal',
  templateUrl: './bought-modal.component.html',
  styleUrls: ['./bought-modal.component.scss']
})
export class BoughtModalComponent implements OnInit {
  @Input() showModal: boolean;
  @Input() item: Item;
  @Output() bought: EventEmitter<Item>;
  @Output() exit: EventEmitter<boolean>;
  faShoppingCart = faShoppingCart;

  constructor() { 
    this.bought =new EventEmitter();
    this.exit = new EventEmitter();
  }

  ngOnInit(): void {
  }

  onBought() {
    this.bought.emit(this.item);
    this.showModal=false;
  }

  onExit(event) {
    this.exit.emit(event);
  }

}
