import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoughtModalComponent } from './bought-modal.component';

describe('BoughtModalComponent', () => {
  let component: BoughtModalComponent;
  let fixture: ComponentFixture<BoughtModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoughtModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoughtModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
