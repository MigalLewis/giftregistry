import { Component, OnInit } from '@angular/core';
import { Item, Group } from '../models/item.model';
import { RegistryService } from '../services/registry.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {
  itemList: Item[];
  lockModal: boolean;
  boughtModal: boolean;
  groupByModal: boolean;
  selectedItem: Item;
  group: Group;
  mobile: boolean;



  constructor(private registryService: RegistryService) { }

  ngOnInit(): void {
    // this.registryService.addDefualtRegistryItems();
    this.registryService.getRegsitry().subscribe(data => {
      this.itemList = data;
    });
    if (window.screen.width <= 600) { // 768px portrait
      this.mobile = true;
    }
  }

  showInterest(item: Item) {
      this.selectedItem = item;
      this.lockModal = true;
  }
  onShowBoughtModal(item: Item) {
      this.selectedItem = item;
      this.boughtModal = true;
      console.log(this.boughtModal);
  }
  onGroupBuy(item: Item) {
    this.selectedItem = item;
    this.registryService.getGroup(this.selectedItem.id).subscribe(data => {
      if(data && data.length > 0) {
        this.group = data[0];
      }
    });
    this.groupByModal = true;
}

  onLike(item: Item) {
    this.registryService.like(item);
  }
  onLock(item: Item) {
    this.registryService.lock(item);
    this.lockModal=false;
  }
  onBought(item: Item) {
    this.registryService.stateBought(item);
    this.boughtModal=false;
  }
  onAddToGroup(item: Item) {
    this.boughtModal=false;
  }
  getLocked(item: Item) {
    if(!item.locked) {
      return false;
    }
    return item.locked.locked;
  }
  onExitGroupBuy(shouldExit) {
    if(shouldExit) {
      this.groupByModal=false;
    }
  }
  onExitBought(shouldExit) {
    if(shouldExit) {
      this.boughtModal=false;
    }
  }
  onExitLock(shouldExit) {
    if(shouldExit) {
      this.lockModal=false;
    }
  }

  unlimited(item: Item) {
    return item.quantity===-1;
  }



}
