import { Component, OnInit, Input } from '@angular/core';
import { Item, Group } from '../models/item.model';

@Component({
  selector: 'app-group-buy',
  templateUrl: './group-buy.component.html',
  styleUrls: ['./group-buy.component.scss']
})
export class GroupBuyComponent implements OnInit {
  @Input() item: Item;
  group: Group;

  constructor() {
  }

  ngOnInit(): void {
  }

}
