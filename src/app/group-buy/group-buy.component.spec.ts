import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupBuyComponent } from './group-buy.component';

describe('GroupBuyComponent', () => {
  let component: GroupBuyComponent;
  let fixture: ComponentFixture<GroupBuyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupBuyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
