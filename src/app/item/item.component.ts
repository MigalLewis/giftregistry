import { Component, OnInit, Input } from '@angular/core';
import { faHeart, faGifts, faLock } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  @Input() name: string;
  @Input() description: string;
  @Input() image: string;
  @Input() like: number;
  @Input() taken: boolean;
  @Input() locked: boolean;
  faHeart = faHeart;
  faGift = faGifts;
  faLock = faLock;

  constructor() { }

  ngOnInit(): void {
  }

}
