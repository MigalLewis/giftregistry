import { Injectable, EventEmitter } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  loadingEmitter: EventEmitter<boolean>;
  constructor() {this.loadingEmitter = new EventEmitter(false); }
  startSpinner() {
    this.loadingEmitter.emit(true);
  }
  stopSpinner() {
    this.loadingEmitter.emit(false);
  }

  isLoading() {
    return this.loadingEmitter;
  }
}
