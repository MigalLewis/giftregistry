import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '../models/user.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private firestore: AngularFirestore) {
  }

  addUser(user: User, id: string): Promise<void>  {
    return this.firestore.collection<User>(environment.firebaseConfig.data.userUrl).doc(id).set(user);
  }

  updateUser(user: User, id: string): Promise<void>  {
    return this.firestore.collection<User>(environment.firebaseConfig.data.userUrl).doc(id).update(user);
  }
}
