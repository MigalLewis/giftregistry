import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Item, Group } from '../models/item.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from './authService';
import { User } from '../models/user.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegistryService {

  DEFAULT_REGISTRY = '../../assets/data/registry.json';

  constructor(private firestore: AngularFirestore,
    private httpClient: HttpClient,
    public auth: AngularFireAuth,
    private authService: AuthService) { }

  getRegsitry():Observable<Item[]> {
    return this.firestore.collection<Item>(environment.firebaseConfig.data.registryUrl).valueChanges();
  }
  // getRegsitry():Observable<Item[]> {
  //   return this.httpClient.get<Item[]>(this.DEFAULT_REGISTRY);
  // }

  public addDefualtRegistryItems() {
    this.httpClient.get<Item[]>(this.DEFAULT_REGISTRY).subscribe((data: Item[]) => {
      data.forEach(item => {
        this.addRegistyItem(item);
      });
    });
  }
  resetLocks() {
    this.getRegsitry().subscribe(data => {
      data.forEach((item: Item) => {
        
        item.locked = {
          locked: false
        }
        this.updateRegistryItem(item,item.id);
      });
    })
  }
  public addRegistyItem(registryItem: Item) {
    this.firestore.collection<Item>(environment.firebaseConfig.data.registryUrl).add(registryItem).then((docRef:DocumentReference)=>{
      registryItem.id = docRef.id;
      this.updateRegistryItem(registryItem,docRef.id);
    });
  }

  updateRegistryItem(registryItem: Item, id: string){
    this.firestore.collection<Item>(environment.firebaseConfig.data.registryUrl).doc(id).update(registryItem);
  }
  like(item: Item) {
    this.auth.currentUser.then((data: firebase.User)=> {
      let stringUser = window.localStorage.getItem('user');
      let user: User = JSON.parse(stringUser);
      if(!user.likedItems){
        user.likedItems= [];
      }
      user.likedItems.push(item);
      this.authService.updateUser(user, data.uid).then(data=>{
        if(!item.likes){
          item.likes= 0;
        }
        item.likes += 1;
        this.updateRegistryItem(item,item.id);
      });
    });
  }
  lock(item: Item) {
    this.auth.currentUser.then((data: firebase.User)=> {
      let stringUser = window.localStorage.getItem('user');
      let user: User = JSON.parse(stringUser);
      if(!user.lockedItems){
        user.lockedItems= [];
      }
      user.lockedItems.push(item);
      let uid = data.uid;
      this.authService.updateUser(user, uid).then(data=>{
        item.locked = {
          dateLocked: new Date(),
          locked: true,
          userId: uid
        };
        this.updateRegistryItem(item,item.id);
      });
    });
  }
  stateBought(item: Item) {
    this.auth.currentUser.then((data: firebase.User)=> {
      let stringUser = window.localStorage.getItem('user');
      let user: User = JSON.parse(stringUser);
      user.boughtItem = item;
      this.authService.updateUser(user, data.uid).then(data=>{
        item.available = false;
        this.updateRegistryItem(item,item.id);
      });
    });
  }

  joinGroup(group: Group, id: string) {
    return this.firestore.collection<Group>(environment.firebaseConfig.data.groupBuyUrl).doc(id).update(group);
  }
  createGroup(group: Group, item: Item): Promise<void> {
    this.updateRegistryItem(item, item.id);
    return this.firestore.collection<Group>(environment.firebaseConfig.data.groupBuyUrl).doc(item.id).set(group);
  }
  getGroup(id: string):Observable<Group[]> {
    return this.firestore.collection<Group>(environment.firebaseConfig.data.groupBuyUrl, ref => ref
    .where('itemId', '==', id)).valueChanges();
  }


}
