import { Injectable, EventEmitter } from '@angular/core';
import { Alert } from '../models/alert.model';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  modalEmitter: EventEmitter<boolean>;

  constructor() {
    this.modalEmitter = new EventEmitter();
  }

  addAlert(shouldOpen: boolean) {
    this.modalEmitter.emit(shouldOpen);
  }

  getModalOpen(): EventEmitter<boolean> {
    return this.modalEmitter;
  }
}
