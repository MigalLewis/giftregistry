import { Injectable, EventEmitter } from '@angular/core';
import { Alert } from '../models/alert.model';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  alerts: EventEmitter<Alert>;

  constructor() {
    this.alerts = new EventEmitter();
  }

  addAlert(alert: Alert) {
    this.alerts.emit(alert);
  }

  getAlerts(): EventEmitter<Alert> {
    return this.alerts;
  }
}
