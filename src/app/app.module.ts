import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { ItemComponent } from './item/item.component';
import { ItemListComponent } from './item-list/item-list.component';
import { FindAtComponent } from './find-at/find-at.component';
import { ActionsComponent } from './actions/actions.component';
import { HeaderComponent } from './header/header.component';
import { LoadingComponent } from './loading/loading.component';
import { InterestComponent } from './interest/interest.component';
import { ActionModalComponent } from './action-modal/action-modal.component';
import { BoughtModalComponent } from './bought-modal/bought-modal.component';
import { GroupBuyModalComponent } from './group-buy-modal/group-buy-modal.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GroupBuyComponent } from './group-buy/group-buy.component';
import { GroupComponent } from './group/group.component';
import { TempLoginComponent } from './temp-login/temp-login.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ItemComponent,
    ItemListComponent,
    FindAtComponent,
    ActionsComponent,
    HeaderComponent,
    LoadingComponent,
    InterestComponent,
    ActionModalComponent,
    BoughtModalComponent,
    GroupBuyModalComponent,
    GroupBuyComponent,
    GroupComponent,
    TempLoginComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    FontAwesomeModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
