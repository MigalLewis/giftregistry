// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBE_Exc5ONBGSoq7x-7Bw57JiJ-FLGuS1I",
    authDomain: "giftregistry-a6abb.firebaseapp.com",
    databaseURL: "https://giftregistry-a6abb.firebaseio.com",
    projectId: "giftregistry-a6abb",
    storageBucket: "giftregistry-a6abb.appspot.com",
    messagingSenderId: "705004189462",
    appId: "1:705004189462:web:67de2bfaab2d4874228237",
    measurementId: "G-18846QW6ZY",
    data: {
      registryUrl: "registry-dev",
      groupBuyUrl: "group-dev",
      userUrl: "user-dev"
    }
  },
  auth: {
    returnUrl: "http://localhost:4200/pre/login"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
