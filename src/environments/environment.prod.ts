export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyBE_Exc5ONBGSoq7x-7Bw57JiJ-FLGuS1I",
    authDomain: "giftregistry-a6abb.firebaseapp.com",
    databaseURL: "https://giftregistry-a6abb.firebaseio.com",
    projectId: "giftregistry-a6abb",
    storageBucket: "giftregistry-a6abb.appspot.com",
    messagingSenderId: "705004189462",
    appId: "1:705004189462:web:67de2bfaab2d4874228237",
    measurementId: "G-18846QW6ZY",
    data: {
      registryUrl: "registry",
      groupBuyUrl: "group",
      userUrl: "user"
    }
  },
  auth: {
    returnUrl: "https://giftregistry-a6abb.web.app/pre/login"
  }
};
